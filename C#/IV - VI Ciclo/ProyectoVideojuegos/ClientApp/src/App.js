
//Importancion
import { useEffect, useState } from "react"
import { Container, Row, Col, Card, CardHeader, CardBody, Button } from "reactstrap"
import TablaVideojuego from "./componentes/TablaVideojuego"
import ModalVideojuego from "./componentes/ModalVideojuego"


const App = () => {

    //funciones
    const [videojuegos, setVideojuegos] = useState([])
    const [mostrarModal, setMostrarModal] = useState(false);
    const [editar, setEditar] = useState(null)


    //mostrar videojuegos
    const mostrarVideojuegos = async () => {
        const response = await fetch("api/videojuego/Lista");

        if (response.ok) {
            const data = await response.json();
            setVideojuegos(data)
        } else {
            console.log("error en la lista")
        }

    }


    useEffect(() => {
        mostrarVideojuegos()
    }, [])

    //Guardar videojuegos
    const guardarVideojuego = async (videojuego) => {

        const response = await fetch("api/videojuego/Guardar",
            {
                method: "POST",
                headers: {
                    "Content-Type" : "application/json;charset=utf-8"
                },
                body: JSON.stringify(videojuego)
            }
        )

        if (response.ok) {
            setMostrarModal(!mostrarModal);
            mostrarVideojuegos();
        }
    }


    //Editar Videojuegos
    const editarVideojuego = async (videojuego) => {

        const response = await fetch("api/videojuego/Editar",
            {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json;charset=utf-8"
                },
                body: JSON.stringify(videojuego)
            }
        )

        if (response.ok) {
            setMostrarModal(!mostrarModal);
            mostrarVideojuegos();
        }
    }


    //Eliminar Videojuego
    const eliminarVideojuego = async (id) => {

        var respuesta = window.confirm("�desea eliminar el videojuego?")

        if (!respuesta) {
            return;
        } 


        const response = await fetch("api/videojuego/Eliminar/" + id, {
            method: "DELETE",
        })

        if (response.ok) {
            mostrarVideojuegos();
        }
    }
    
    //Pagina Html
    return (
        <Container>
            <Row className="mt-5">

                <Col sm="12">
                    <Card>
                        <CardHeader>
                            <h5>Lista de Videojuegos</h5>
                        </CardHeader>
                        <CardBody>
                            <Button size="sm" color="success" onClick={() => setMostrarModal(!mostrarModal)}>Nuevo Videojuego</Button>
                            <hr></hr>
                            <TablaVideojuego data={videojuegos}
                                setEditar={setEditar}
                                mostrarModal={mostrarModal}
                                setMostrarModal={setMostrarModal}



                                eliminarVideojuego={eliminarVideojuego}
                            />
                        </CardBody>
                    </Card>
                </Col>

            </Row>
            <ModalVideojuego
                mostrarModal={mostrarModal}
                setMostrarModal={setMostrarModal}
                guardarVideojuego={guardarVideojuego}


                editar={editar}
                setEditar={setEditar}
                editarVideojuego={editarVideojuego}
            />
        </Container>
    )
}

export default App;