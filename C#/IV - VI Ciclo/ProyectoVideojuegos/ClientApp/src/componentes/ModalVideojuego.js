﻿// Importación de módulos de la biblioteca Reactstrap y React.
import { Modal, ModalBody, ModalHeader, Form, FormGroup, Input, Label, ModalFooter, Button } from "reactstrap"
import { useEffect, useState } from "react"


// Objeto que representa un videojuego con sus propiedades.
const modeloVideojuego = {
    id : 0,
    titulo : "",
    plataforma : "",
    desarrollador : "",
    fecha : "",
    genero : "",
    precio : ""
}

// Componente que representa un modal para agregar o editar un videojuego.
const ModelVideojuego = ({ mostrarModal, setMostrarModal, guardarVideojuego, editar, setEditar, editarVideojuego }) => {


    // Estado del videojuego.
    const [videojuego, setVideojuego] = useState(modeloVideojuego);


    // Función para actualizar el valor de una propiedad del videojuego.
    const actualizarDato = (e) => {
        console.log(e.target.name + " : " + e.target.value)
        setVideojuego(
            {
                ...videojuego,
                [e.target.name]: e.target.value
            }
        )
    }


    // Función para guardar o editar el videojuego.
    const enviarDatos = () => {

        if (videojuego.id == 0) {
            guardarVideojuego(videojuego)
        } else {
            editarVideojuego(videojuego)
        }

        setVideojuego(modeloVideojuego)

    }

    // Efecto para actualizar el estado del videojuego al editar.
    useEffect(() => {
        if (editar != null) {
            setVideojuego(editar)
        } else {
            setVideojuego(modeloVideojuego)
        }
    },[editar])


    // Función para cerrar el modal.
    const cerrarModel = () => {

        setMostrarModal(!mostrarModal)
        setEditar(null)

    }


    // Retorno del componente con el modal.
    return (
        <Modal isOpen={ mostrarModal }>
            <ModalHeader>
                {videojuego.id == 0 ? "Nuevo Videojuego" : "Editar Contacto" }
                
            </ModalHeader>
            <ModalBody>
                <Form>
                    <FormGroup>
                        <Label>Titulo</Label>
                        <Input name="titulo" onChange={(e) => actualizarDato(e)} value={videojuego.titulo} />
                    </FormGroup>
                    <FormGroup>
                        <Label>Plataforma</Label>
                        <Input name="plataforma" onChange={(e) => actualizarDato(e)} value={videojuego.plataforma} />
                    </FormGroup>  
                    <FormGroup>
                        <Label>Desarrollador</Label>
                        <Input name="desarrollador" onChange={(e) => actualizarDato(e)} value={videojuego.desarrollador} />
                    </FormGroup>  
                    <FormGroup>
                        <Label>Fecha de Lanzamiento</Label>
                        <Input name="fecha" onChange={(e) => actualizarDato(e)} value={videojuego.fecha} />
                    </FormGroup>  
                    <FormGroup>
                        <Label>Genero</Label>
                        <Input name="genero" onChange={(e) => actualizarDato(e)} value={videojuego.genero} />
                    </FormGroup>  
                    <FormGroup>
                        <Label>Precio</Label>
                        <Input name="precio" onChange={(e) => actualizarDato(e)} value={videojuego.precio} />
                    </FormGroup>  
                </Form>
            </ModalBody>
            <ModalFooter>
                <Button color="primary" size="sm" onClick={enviarDatos}>Guardar</Button>
                <Button color="danger" size="sm" onClick={cerrarModel}>Cerrar</Button>
            </ModalFooter>
        </Modal>

        )
}

export default ModelVideojuego;