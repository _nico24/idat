﻿
//Importacion
import { Table, Button } from "reactstrap"

//Funcion
const TablaVideojuego = ({ data, setEditar, mostrarModal, setMostrarModal, eliminarVideojuego }) => {

    const enviarDatos = (videojuego) => {
        setEditar(videojuego)
        setMostrarModal(!mostrarModal)
    }

    //Tabla Html 
    return (
        <Table striped responsive>
            <thead>
                <tr>

                    <th>TITULO</th>
                    <th>PLATAFORMA</th>
                    <th>DESARROLLADOR</th>
                    <th>FECHA DE LANZAMIENTO</th>
                    <th>GENERO</th>
                    <th>PRECIO</th>
                </tr>
            </thead>
            <tbody>
                {
                    (data.length < 1) ? (
                        <tr>
                            <td colSpan="4">Sin registros</td>
                        </tr>
                    ) : (
                        data.map((item) => (

                            //Mostrando los datos de la base de datos en la tabla
                            <tr key={item.id}>
                                <td>{item.titulo}</td>
                                <td>{item.plataforma}</td>
                                <td>{item.desarrollador}</td>
                                <td>{item.fecha}</td>
                                <td>{item.genero}</td>
                                <td>{item.precio}</td>
                                <td>
                                    <Button color="primary" size="sm" className="me-2"
                                        onClick={() => enviarDatos(item)}
                                    >Editar</Button>
                                    <Button color="danger" size="sm"
                                        onClick={() => eliminarVideojuego(item.id)}
                                    >Eliminar</Button>
                                </td>
                            </tr>

                        ))
                    )

                }
            </tbody>
        </Table>
    )

}

export default TablaVideojuego;