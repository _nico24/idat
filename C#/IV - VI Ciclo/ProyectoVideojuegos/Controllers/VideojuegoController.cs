﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProyectoVideojuegos.Models;


namespace Videojuegos.Controllers
{
    //API
    [Route("api/[controller]")]
    [ApiController]
    public class VideojuegoController : ControllerBase
    {
        private readonly VideojuegosContext _dbcontext;

        public VideojuegoController(VideojuegosContext context)
        {
            _dbcontext = context;
        }

        //FUNCION LISTAR
        [HttpGet]
        [Route("Lista")]
        public async Task<IActionResult> Lista()
        {

            List<Videojuego> lista = await _dbcontext.Videojuegos.OrderByDescending(c => c.Id).ToListAsync();

            return StatusCode(StatusCodes.Status200OK, lista);

        }

        //FUNCION GUARDAR
        [HttpPost]
        [Route("Guardar")]
        public async Task<IActionResult> Guardar([FromBody] Videojuego request)
        {

            await _dbcontext.Videojuegos.AddAsync(request);
            await _dbcontext.SaveChangesAsync();

            return StatusCode(StatusCodes.Status200OK, "ok");
        }

        //FUNCION EDITAR
        [HttpPut]
        [Route("Editar")]
        public async Task<IActionResult> Editar([FromBody] Videojuego request)
        {

            _dbcontext.Videojuegos.Update(request);
            await _dbcontext.SaveChangesAsync();

            return StatusCode(StatusCodes.Status200OK, "ok");
        }

        //FUNCION ELIMINAR
        [HttpDelete]
        [Route("Eliminar/{id:int}")]
        public async Task<IActionResult> Eliminar(int id)
        {

            Videojuego videojuego = _dbcontext.Videojuegos.Find(id);

            _dbcontext.Videojuegos.Remove(videojuego);
            await _dbcontext.SaveChangesAsync();

            return StatusCode(StatusCodes.Status200OK, "ok");
        }


    }
}
