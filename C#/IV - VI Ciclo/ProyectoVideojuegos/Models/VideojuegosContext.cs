﻿using Microsoft.EntityFrameworkCore;

namespace ProyectoVideojuegos.Models;

public partial class VideojuegosContext : DbContext
{
    public VideojuegosContext()
    {
    }

    public VideojuegosContext(DbContextOptions<VideojuegosContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Videojuego> Videojuegos { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Data Source=DESKTOP-21EENR1;Initial Catalog=Videojuegos;Integrated Security=SSPI; User ID=sa;Password=1234; integrated security=True;Encrypt=False;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Videojuego>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Videojue__3213E83F17C5A3E6");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Desarrollador)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasColumnName("desarrollador");
            entity.Property(e => e.Fecha)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasColumnName("fecha");
            entity.Property(e => e.Genero)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("genero");
            entity.Property(e => e.Plataforma)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("plataforma");
            entity.Property(e => e.Precio)
                .HasColumnType("decimal(10, 2)")
                .HasColumnName("precio");
            entity.Property(e => e.Titulo)
                .HasMaxLength(255)
                .IsUnicode(false)
                .HasColumnName("titulo");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
