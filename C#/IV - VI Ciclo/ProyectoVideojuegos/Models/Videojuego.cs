﻿namespace ProyectoVideojuegos.Models;
//TABLA
public partial class Videojuego
{
    public int Id { get; set; }

    public string Titulo { get; set; } = null!;

    public string Plataforma { get; set; } = null!;

    public string Desarrollador { get; set; } = null!;

    public string Fecha { get; set; } = null!;

    public string? Genero { get; set; }

    public decimal? Precio { get; set; }
}
