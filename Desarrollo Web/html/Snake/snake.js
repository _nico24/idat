// Configuración del juego
const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");
const box = 10;
const snake = [{ x: 20, y: 20 }];
let direction = "right";
let food = generateFood();
let score = 0;

// Dibujar elementos en el canvas
function drawSnake() {
  for (let i = 0; i < snake.length; i++) {
    ctx.fillStyle = i === 0 ? "green" : "white";
    ctx.fillRect(snake[i].x, snake[i].y, box, box);
    ctx.strokeStyle = "red";
    ctx.strokeRect(snake[i].x, snake[i].y, box, box);
  }
}

function drawFood() {
  ctx.fillStyle = "red";
  ctx.fillRect(food.x, food.y, box, box);
}

function drawScore() {
  ctx.fillStyle = "black";
  ctx.font = "20px Arial";
  ctx.fillText("Score: " + score, canvas.width - 100, 30);
}

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  drawSnake();
  drawFood();
  drawScore();
}

// Funciones de movimiento y colisión
function moveSnake() {
  let head = { x: snake[0].x, y: snake[0].y };
  if (direction === "right") head.x += box;
  if (direction === "left") head.x -= box;
  if (direction === "up") head.y -= box;
  if (direction === "down") head.y += box;
  if (head.x === food.x && head.y === food.y) {
    food = generateFood();
    score += 10;
  } else {
    snake.pop();
  }
  snake.unshift(head);
}

function generateFood() {
  return {
    x: Math.floor(Math.random() * (canvas.width / box)) * box,
    y: Math.floor(Math.random() * (canvas.height / box)) * box,
  };
}

function checkCollision() {
  let head = snake[0];
  if (
    head.x < 0 ||
    head.x >= canvas.width ||
    head.y < 0 ||
    head.y >= canvas.height
  ) {
    return true;
  }
  for (let i = 1; i < snake.length; i++) {
    if (head.x === snake[i].x && head.y === snake[i].y) {
      return true;
    }
  }
  return false;
}

// Eventos del teclado
document.addEventListener("keydown", (event) => {
  if (event.keyCode === 37 && direction !== "right") {
    direction = "left";
  }
  if (event.keyCode === 38 && direction !== "down") {
    direction = "up";
  }
  if (event.keyCode === 39 && direction !== "left") {
    direction = "right";
  }
  if (event.keyCode === 40 && direction !== "up") {
    direction = "down";
  }
});

// Loop principal
function gameLoop() {
    moveSnake();
    if (checkCollision()) {
    clearInterval(gameInterval);
    alert("Game over!");
    }
    draw();
    }
    
    let gameInterval = setInterval(gameLoop, 100); // velocidad del juego