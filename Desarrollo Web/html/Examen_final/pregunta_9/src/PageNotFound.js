import React from 'react';

function PageNotFound() {
  return (
    <div>
      <h1>Error 404: Página no encontrada</h1>
      <p>Lo siento, la página que estás buscando no existe.</p>
    </div>
  );
}

export default PageNotFound;
