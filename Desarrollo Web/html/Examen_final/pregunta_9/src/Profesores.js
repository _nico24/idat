import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function Inicio() {
  return (
    <div>
      <h1 className="welcome">Profesores</h1>
      <p className="description">Revisa encontraras los datos de tus profesores</p>
      <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center' }}>
        <img id="animated-image" src="https://img.freepik.com/vector-gratis/ejemplo-lindo-arte-historieta-caracter-profesor-mujer_56104-780.jpg" alt="" style={{ width: '200px', height: 'auto', marginRight: '10px' }} />
        <img id="animated-image" src="https://www.cinconoticias.com/wp-content/uploads/tipos-de-profesores.1.jpg" alt="" style={{ width: '200px', height: 'auto', marginRight: '10px' }} />
        <img id="animated-image" src="https://plustatic.com/5550/conversions/tipos-profesores-large.jpg" alt="" style={{ width: '200px', height: 'auto', marginRight: '10px' }} />
      </div>
    </div>
  );
}

export default Inicio;
