import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function Inicio() {
  return (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
      <div>
        <h1 className="welcome">Cursos</h1>
        <p className="description">Aqui encontraras los cursos a llevar</p>
        <img id="animated-image" src="https://png.pngtree.com/element_our/20190531/ourmid/pngtree-flat-desk-decoration-illustration-image_1293460.jpg" alt="" style={{ margin: 'auto' }}></img>
      </div>
    </div>
  );
}

export default Inicio;
