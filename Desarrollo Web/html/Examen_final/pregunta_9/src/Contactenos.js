import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function Inicio() {
  return (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
      <div>
        <h1 className="welcome">Contactenos</h1>
        <p className="description">Aqui encontraras la seccion de contactenos</p>
        <img id="animated-image" src="https://jcmt.edu.pe/portal/wp-content/uploads/2019/11/Contact-1024x477.jpg" alt="" style={{ margin: 'auto' }}></img>
      </div>
    </div>
  );
}

export default Inicio;
