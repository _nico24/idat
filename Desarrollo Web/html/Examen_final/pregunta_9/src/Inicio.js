import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function Inicio() {
  return (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
      <div>
        <h1 className="welcome">Bienvenido</h1>
        <p className="description">Hola Nicolas Alexis Badajos Rojas</p>
        <p className="description">Este es el panel principal</p>
        <img id="animated-image" src="https://cdn5.dibujos.net/dibujos/pintados/202032/computadora-la-casa-la-habitacion-11917349.jpg" alt="" style={{ margin: 'auto' }}></img>
      </div>
    </div>
  );
}

export default Inicio;
