import { Link } from 'react-router-dom';
import "./App.css";

function Navbar() {
  return (
    <nav className="nav-bar">
      <ul className="nav-links">
        <li>
          <Link to="/" className="link">Inicio</Link>
        </li>
        <li>
          <Link to="/curso" className="link">Cursos</Link>
        </li>
        <li>
          <Link to="/profesores" className="link">Profesores</Link>
        </li>
        <li>
          <Link to="/pagos" className="link">Mis Pagos</Link>
        </li>
        <li>
          <Link to="/contactenos" className="link">Contactenos</Link>
        </li>
      </ul>
    </nav>
  );
}

export default Navbar;
