import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Navbar from './Navbar';
import Cursos from './Cursos';
import Profesores from './Profesores';
import Inicio from './Inicio';
import Pagos from "./Pagos";
import Contactenos from './Contactenos';
import PageNotFound from './PageNotFound';

import './App.css';

function App() {
  return (
    <Router>
      <div className="App">
        <Navbar />
        <Routes>
          <Route path="/" element={<Inicio />} />
          <Route path="/curso" element={<Cursos />} />
          <Route path="/profesores" element={<Profesores />} />
          <Route path="/pagos" element={<Pagos />} />
          <Route path="/contactenos" element={<Contactenos />} />
          <Route path="*" element={<PageNotFound />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
