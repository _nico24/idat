import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function Inicio() {
  return (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
      <div>
        <h1 className="welcome">Pagos</h1>
        <p className="description">Revisa aqui todos tus pagos</p>
        <img id="animated-image" src="https://geekflare.com/wp-content/uploads/2022/09/Payment-Processing-Solutions.jpg" alt="" style={{ margin: 'auto' }}></img>
      </div>
    </div>
  );
}

export default Inicio;
