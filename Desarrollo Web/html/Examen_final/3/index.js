let texto = prompt("Por favor, ingresa una frase");

let palabras = texto.split(" ");
let numPalabras = palabras.length;

let primeraPalabra = palabras[0];
let ultimaPalabra = palabras[numPalabras - 1];

let palabrasInversas = palabras.reverse().join(" ");
let palabrasOrdenadasAZ = palabras.sort().join(" ");
let palabrasOrdenadasZA = palabras.sort((a, b) => b.localeCompare(a)).join(" ");

console.log(`Número de palabras: ${numPalabras}`);
console.log(`Primera palabra: ${primeraPalabra}`);
console.log(`Última palabra: ${ultimaPalabra}`);
console.log(`Palabras en orden inverso: ${palabrasInversas}`);
console.log(`Palabras ordenadas de la a la z: ${palabrasOrdenadasAZ}`);
console.log(`Palabras ordenadas de la z a la a: ${palabrasOrdenadasZA}`);

let textoSinEspacios = texto.toLowerCase().replace(/\s/g, "");
let textoInvertido = textoSinEspacios.split("").reverse().join("");
if (textoSinEspacios === textoInvertido) {
  console.log("La frase es un palíndromo");
} else {
  console.log("La frase no es un palíndromo");
}
