$(document).ready(function(){
 
  var oneClicked = false;
  $("#one-button").click(function(){
    if (!oneClicked) {
      alert("Bienvenido al jQuery");
      oneClicked = true;
    }
  });

  $("#css-button").click(function(){
    $("header").css("background-color", "#ff9900");
  });

  $("#animate-button").click(function(){
    $("#animated-image").animate({
      opacity: 0.5,
      borderWidth: "5px"
    }, 500);
  });
});