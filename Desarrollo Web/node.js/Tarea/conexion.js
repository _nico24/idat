var con = mysql.createConnection(jConexion); 
//Abrimos conexión a la base de datos, "connect" regresa un error si no podemos
//conectarnos a la base de datos
con.connect(function(error){                                                                                                 
    try { 
        //si error es true mandamos el mensaje de error
        if(error){ 
            console.log("Error al establecer la conexión a la BD -- " + error);
        }else{  
            //conexión exitosa, en este punto ya hemos establecido la conexión a 
            //base de datos
            console.log("Conexión exitosa"); 
            //Aquí debes escribir el código que necesites, un INSERT, SELECT etc.
        } 
    } catch(x){ 
        console.log("Contacto.agregarUsuario.connect --Error-- " + x); 
    } 
});
