//?	¿Qué es y para qué sirve Node JS? 
//!Node.js es un entorno de ejecución de JavaScript construido sobre el motor V8 de Google Chrome. Es una plataforma que permite ejecutar código JavaScript en el lado del servidor y no en el navegador, lo que la hace ideal para crear aplicaciones web escalables y de alta performance.


//?	¿Cómo se conecta Node JS con MySQL?
//!Primero, se debe instalar el paquete mysql ejecutando el siguiente comando en la terminal:
//!npm install mysql


//!Luego, se debe crear una conexión a la base de datos de MySQL con los siguientes parámetros:
const mysql = require('mysql');

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'usuario',
  password: 'contraseña',
  database: 'nombre_base_datos'
});


//!Una vez que se ha establecido la conexión a la base de datos, se pueden ejecutar consultas utilizando la función query:
connection.query('SELECT * FROM tabla', (error, results, fields) => {
    if (error) throw error;
    console.log('Los resultados son: ', results);
  });

  
//!Finalmente, se debe cerrar la conexión a la base de datos cuando ya no sea necesaria:
  connection.end();

//? ¿Cuál es el mejor ORM de MySQL para Node js? De una breve explicación
//!Hay varios ORM (Object-Relational Mapping) populares para Node.js y MySQL, cada uno con sus propias ventajas y desventajas.

//! - Sequelize
//! - TypeORM
//! - Knex
//! - Bookshelf
//! Sequelize es probablemente la opción más popular y completa en la actualidad, con una amplia comunidad de desarrolladores y una amplia gama de características y soporte para dialectos de bases de datos.