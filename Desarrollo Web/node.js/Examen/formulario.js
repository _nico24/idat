$(document).ready(function() {
    $('#formulario').validate({
      rules: {
        nombres: {
          required: true,
          minlength: 5,
          maxlength: 20
        },
        email: {
          required: true,
          email: true
        },
        celular: {
          required: true,
          minlength: 9,
          maxlength: 9,
          digits: true
        },
        contraseña: {
          required: true,
          minlength: 6,
          maxlength: 12
        },
        repitaContraseña: {
          required: true,
          minlength: 6,
          maxlength: 12,
          equalTo: '#contraseña'
        }
      },
      messages: {
        nombres: {
          required: 'El campo Nombres es obligatorio.',
          minlength: 'El campo Nombres debe tener al menos 5 caracteres.',
          maxlength: 'El campo Nombres no debe tener más de 20 caracteres.'
        },
        email: {
          required: 'El campo Email es obligatorio.',
          email: 'El campo Email debe ser una dirección de correo electrónico válida.'
        },
        celular: {
          required: 'El campo Celular es obligatorio.',
          minlength: 'El campo Celular debe tener 9 dígitos.',
          maxlength: 'El campo Celular debe tener 9 dígitos.',
          digits: 'El campo Celular debe contener solo dígitos.'
        },
        contraseña: {
          required: 'El campo Contraseña es obligatorio.',
          minlength: 'El campo Contraseña debe tener al menos 6 caracteres.',
          maxlength: 'El campo Contraseña no debe tener más de 12 caracteres.'
        },
        repitaContraseña: {
          required: 'El campo Repita contraseña es obligatorio.',
          minlength: 'El campo Repita contraseña debe tener al menos 6 caracteres.',
          maxlength: 'El campo Repita contraseña no debe tener más de 12 caracteres.',
          equalTo: 'Las contraseñas deben coincidir.'
        }
      }
    });
  });
