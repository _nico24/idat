const numeros = [];
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
});
readline.question('Por favor ingrese 10 números enteros separados por espacio: ', entrada => {
  numeros.push(...entrada.split(' ').map(numero => parseInt(numero)));
  const numeroMayor = Math.max(...numeros);
  const numeroMenor = Math.min(...numeros);
  console.log(`El número mayor es ${numeroMayor} y el número menor es ${numeroMenor}.`);
  readline.close();
});
