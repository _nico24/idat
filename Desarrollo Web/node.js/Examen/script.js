var cuadrado = document.getElementById("cuadrado");
var posX = 0;
var posY = 0;
document.onkeydown = function(event) {
  switch (event.keyCode) {
    case 37: 
      posX -= 10;
      break;
    case 38: 
      posY -= 10;
      break;
    case 39: 
      posX += 10;
      break;
    case 40: 
      posY += 10;
      break;
  }
  cuadrado.style.top = posY + "px";
  cuadrado.style.left = posX + "px";
};
