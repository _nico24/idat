import React, { Component } from 'react';

class CompProductoResumen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cantidad: 1 // Valor inicial de la cantidad
    };

    this.handleCantidadChange = this.handleCantidadChange.bind(this);
  }

  handleCantidadChange(event) {
    this.setState({
      cantidad: event.target.value
    });
  }

  render() {
    var urlImagen = 'imagenes/' + this.props.pDatosDelProducto.url;
    var urlProducto = 'producto/' + this.props.pDatosDelProducto.id;

    return (
      <div className="col mb-4">
        <div className="card h-100">
          <a href="producto/1">
            <img src={urlImagen} className="card-img-top" alt="..." />
          </a>
          <div className="card-body">
            <a className="text-primary" href={urlProducto}>
              <h5 className="card-title">{this.props.pDatosDelProducto.nombre}</h5>
            </a>
            <p className="card-text">{this.props.pDatosDelProducto.descripcion}</p>
            <p className="text-primary">S/ {this.props.pDatosDelProducto.precio}</p>

            
            <div className="form-group">
              <label htmlFor="cantidad">Cantidad:</label>
              <input
                type="number"
                className="form-control"
                id="cantidad"
                value={this.state.cantidad}
                onChange={this.handleCantidadChange}
                min="1" 
                max="10" 
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CompProductoResumen;
