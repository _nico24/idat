import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HomeComponent } from './dashboard/home/home.component';
import { RouterModule } from '@angular/router';
import { CursoListaComponent } from './dashboard/curso/curso-lista/curso-lista.component';
import { HttpClientModule } from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    PageNotFoundComponent,
    HomeComponent,
    CursoListaComponent,

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path: "login", component: LoginComponent},
      {path: "dashboard", component: DashboardComponent,
    children:[
      {path: "home", component: HomeComponent},
      {path: "curso", component: CursoListaComponent}
    ]},
      {path: "", redirectTo: "login", pathMatch: "full"},
      {path: "**", component: PageNotFoundComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
