export interface Curso {
    id: string,
    nombre: string,
    credito: number,
    estado: string
}
