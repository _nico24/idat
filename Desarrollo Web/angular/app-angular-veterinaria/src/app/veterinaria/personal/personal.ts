export interface Personal {
    id: string,
    nombre: string,
    cargo: string,
    experiencia: string
}
