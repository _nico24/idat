import { Component } from '@angular/core';
import { Personal } from '../personal';
import { PersonalService } from '../personal.service';

@Component({
  selector: 'app-personal-lista',
  templateUrl: './personal-lista.component.html',
  styleUrls: ['./personal-lista.component.css']
})
export class PersonalListaComponent {

  Personal: Personal[] = []

  constructor(private personalService: PersonalService){}

  ngOnInit(): void {
    this.personalService.personallist().subscribe((data) => {this.Personal = data})
  }

}
