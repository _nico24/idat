import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Personal } from './personal';

@Injectable({
  providedIn: 'root'
})
export class PersonalService {

  constructor(private http: HttpClient) { }

  personallist(): Observable<Personal[]>{
    return this.http.get<Personal[]>("https://643af4dc447794557351f047.mockapi.io/personal")
  }
}
