import { Component } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-veterinaria',
  templateUrl: './veterinaria.component.html',
  styleUrls: ['./veterinaria.component.css']
})
export class VeterinariaComponent {
  constructor(private route: ActivatedRoute, private router: Router){

  }

  onNavigateMenu():void{
    this.router.navigate(["menu"], {relativeTo: this.route})
  }

  onNavigateVenta(): void{
    this.router.navigate(["venta"], {relativeTo: this.route})
  }

  onNavigateAdopcion(): void{
    this.router.navigate(["adopcion"], {relativeTo: this.route})
  }

  onNavigatePersonal(): void{
    this.router.navigate(["personal"], {relativeTo: this.route})
  }

  onNavigateMaterial(): void{
    this.router.navigate(["material"], {relativeTo: this.route})
  }
}
