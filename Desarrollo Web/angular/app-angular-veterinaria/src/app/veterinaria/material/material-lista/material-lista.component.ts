import { Component } from '@angular/core';
import { Material } from '../material';
import { MaterialService } from '../material.service';

@Component({
  selector: 'app-material-lista',
  templateUrl: './material-lista.component.html',
  styleUrls: ['./material-lista.component.css']
})
export class MaterialListaComponent {

  Material: Material[] = []

  constructor(private materialService: MaterialService){}

    ngOnInit(): void {
      this.materialService.materiallist().subscribe((data) => {this.Material = data})
    }

}
