import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Material } from './material';

@Injectable({
  providedIn: 'root'
})
export class MaterialService {

  constructor(private http: HttpClient) { }

  materiallist(): Observable<Material[]>{
    return this.http.get<Material[]>("https://643af4dc447794557351f047.mockapi.io/material")
  }
}
