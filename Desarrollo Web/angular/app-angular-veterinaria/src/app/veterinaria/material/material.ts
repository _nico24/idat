export interface Material {
    id: string,
    material: string,
    cantidad: string,
    descripcion: string
}
