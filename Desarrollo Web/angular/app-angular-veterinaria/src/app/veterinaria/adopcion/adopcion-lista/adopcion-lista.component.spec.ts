import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdopcionListaComponent } from './adopcion-lista.component';

describe('AdopcionListaComponent', () => {
  let component: AdopcionListaComponent;
  let fixture: ComponentFixture<AdopcionListaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdopcionListaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdopcionListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
