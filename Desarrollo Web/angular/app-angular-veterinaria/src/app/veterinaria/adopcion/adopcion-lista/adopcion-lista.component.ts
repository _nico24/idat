import { Component } from '@angular/core';
import { Adopcion } from '../adopcion';
import { AdopcionService } from '../adopcion.service';

@Component({
  selector: 'app-adopcion-lista',
  templateUrl: './adopcion-lista.component.html',
  styleUrls: ['./adopcion-lista.component.css']
})
export class AdopcionListaComponent {

  Adopcion: Adopcion[] = []

  constructor(private adopcionService: AdopcionService){}

    ngOnInit(): void {
     this.adopcionService.adopcionlist().subscribe((data)=>{this.Adopcion = data
    })
    }
}
