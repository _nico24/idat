export interface Adopcion {
    id: string,
    nombre: string,
    especie: string,
    edad: string,
    sexo: string,
    informacion: string
}
