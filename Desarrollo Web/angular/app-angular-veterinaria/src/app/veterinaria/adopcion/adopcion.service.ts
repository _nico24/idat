import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Adopcion } from './adopcion';

@Injectable({
  providedIn: 'root'
})
export class AdopcionService {

  constructor(private http: HttpClient) { }

    adopcionlist(): Observable<Adopcion[]>{
      return this.http.get<Adopcion[]>("https://643aebb290cd4ba563053305.mockapi.io/adopcion")
    }


}
