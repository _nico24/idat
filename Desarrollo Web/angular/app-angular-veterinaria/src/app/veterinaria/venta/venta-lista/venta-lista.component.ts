import { Component } from '@angular/core';
import { Venta } from '../venta';
import { VentaService } from '../venta.service';

@Component({
  selector: 'app-venta-lista',
  templateUrl: './venta-lista.component.html',
  styleUrls: ['./venta-lista.component.css']
})
export class VentaListaComponent {

  Venta: Venta[] = []

  constructor(private ventaService: VentaService){}

  ngOnInit(): void {
   this.ventaService.ventalist().subscribe((data)=>{this.Venta = data})
  }

}
