export interface Venta {
    id: string,
    fecha: string,
    cliente: string,
    producto: string,
    cantidad: string,
    total: string
}
