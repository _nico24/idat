import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Venta } from './venta';

@Injectable({
  providedIn: 'root'
})
export class VentaService {

  constructor(private http: HttpClient) { }

  ventalist(): Observable<Venta[]>{
    return this.http.get<Venta[]>("https://643aebb290cd4ba563053305.mockapi.io/venta")
  }
}
