import { Component } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-logeo',
  templateUrl: './logeo.component.html',
  styleUrls: ['./logeo.component.css']
})
export class LogeoComponent {

  constructor(private router: Router){

  }
  onSubmit(): void{
    this.router.navigate(["veterinaria/menu"]);
  }
}
