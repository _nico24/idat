import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from "@angular/common/http";
import { AppComponent } from './app.component';
import { NoExisteComponent } from './no-existe/no-existe.component';
import { VeterinariaComponent } from './veterinaria/veterinaria.component';
import { MenuComponent } from './veterinaria/menu/menu.component';
import { PersonalListaComponent } from './veterinaria/personal/personal-lista/personal-lista.component';
import { MaterialListaComponent } from './veterinaria/material/material-lista/material-lista.component';
import { AdopcionListaComponent } from './veterinaria/adopcion/adopcion-lista/adopcion-lista.component';
import { VentaListaComponent } from './veterinaria/venta/venta-lista/venta-lista.component';
import { FormsModule } from '@angular/forms';
import { LogeoComponent } from './autentificacion/logeo/logeo.component';



@NgModule({
  declarations: [
    AppComponent,
    NoExisteComponent,
    VeterinariaComponent,
    MenuComponent,
    PersonalListaComponent,
    MaterialListaComponent,
    AdopcionListaComponent,
    VentaListaComponent,
    LogeoComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path: "logeo", component: LogeoComponent},
      {path: "veterinaria", component: VeterinariaComponent,
      children:[
      {path: "adopcion", component: AdopcionListaComponent},
      {path: "material", component: MaterialListaComponent},
      {path: "menu", component: MenuComponent},
      {path: "personal", component: PersonalListaComponent},
      {path: "venta", component: VentaListaComponent}
      ]},
      {path: "", redirectTo: "logeo", pathMatch: "full"},
      {path: "**", component: NoExisteComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
