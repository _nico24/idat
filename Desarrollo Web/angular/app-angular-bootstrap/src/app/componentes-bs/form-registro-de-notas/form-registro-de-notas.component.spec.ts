import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormRegistroDeNotasComponent } from './form-registro-de-notas.component';

describe('FormRegistroDeNotasComponent', () => {
  let component: FormRegistroDeNotasComponent;
  let fixture: ComponentFixture<FormRegistroDeNotasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormRegistroDeNotasComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormRegistroDeNotasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
