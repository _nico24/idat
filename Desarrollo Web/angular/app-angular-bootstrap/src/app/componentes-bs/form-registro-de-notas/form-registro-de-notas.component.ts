import { Component } from '@angular/core';

interface Estudiante {
  nombre: string;
  matematica: number;
  comunicacion: number;
  psocial: number;
  ccnn: number;
  edfisica: number;
  edartistica: number;
  edreligion: number;
}

@Component({
  selector: 'app-form-registro-de-notas',
  templateUrl: './form-registro-de-notas.component.html',
  styleUrls: ['./form-registro-de-notas.component.css']
})
export class FormRegistroDeNotasComponent {
  matematica: number = 0;
  comunicacion: number = 0;
  psocial: number = 0;
  ccnn: number = 0;
  edfisica: number = 0;
  edartistica: number = 0;
  edreligion: number = 0;
  estudiantes: Estudiante[] = [];
  
  agregarEstudiante(nombre: string) {
    const estudiante: Estudiante = {
      nombre,
      matematica: this.matematica,
      comunicacion: this.comunicacion,
      psocial: this.psocial,
      ccnn: this.ccnn,
      edfisica: this.edfisica,
      edartistica: this.edartistica,
      edreligion: this.edreligion
    };
    this.estudiantes.push(estudiante);
    this.matematica = 0;
    this.comunicacion = 0;
    this.psocial = 0;
    this.ccnn = 0;
    this.edfisica = 0;
    this.edartistica = 0;
    this.edreligion = 0;
  }
}
