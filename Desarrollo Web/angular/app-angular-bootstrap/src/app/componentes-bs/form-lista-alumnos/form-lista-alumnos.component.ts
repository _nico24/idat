import { Component } from '@angular/core';

@Component({
  selector: 'app-form-lista-alumnos',
  templateUrl: './form-lista-alumnos.component.html',
  styleUrls: ['./form-lista-alumnos.component.css']
})
export class FormListaAlumnosComponent {



    lstAlumnos = [
      {id: 1, nombre: "Nicolas", apellido: "Badajos", estado: "Activo"},
      {id: 2, nombre: "Alexis", apellido: "Rojas", estado: "Activo"},
      {id: 3, nombre: "Marcos", apellido: "Rivera", estado: "Activo"},
      {id: 4, nombre: "Helen", apellido: "Martinez", estado: "Activo"},
      {id: 5, nombre: "Angelica", apellido: "Kuga", estado: "Activo"},
      {id: 6, nombre: "Karla", apellido: "Palacios", estado: "Activo"},
    ]
}
