import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormRegistroDeDocentesComponent } from './form-registro-de-docentes.component';

describe('FormRegistroDeDocentesComponent', () => {
  let component: FormRegistroDeDocentesComponent;
  let fixture: ComponentFixture<FormRegistroDeDocentesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormRegistroDeDocentesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormRegistroDeDocentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
