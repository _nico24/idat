import { Component } from '@angular/core';

@Component({
  selector: 'app-form-operacion',
  templateUrl: './form-operacion.component.html',
  styleUrls: ['./form-operacion.component.css']
})
export class FormOperacionComponent {
  lstAlumnos = [
    {id: 1, nombre: "Nicolas", apellido: "Badajos", estado: "Activo"},
    {id: 2, nombre: "Alexis", apellido: "Rojas", estado: "Activo"},
    {id: 3, nombre: "Marcos", apellido: "Rivera", estado: "Activo"},
    {id: 4, nombre: "Helen", apellido: "Martinez", estado: "Activo"},
    {id: 5, nombre: "Angelica", apellido: "Kuga", estado: "Activo"},
    {id: 6, nombre: "Karla", apellido: "Palacios", estado: "Activo"},
  ]
}
