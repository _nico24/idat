import { Component } from '@angular/core';

@Component({
  selector: 'app-form-directivas',
  templateUrl: './form-directivas.component.html',
  styleUrls: ['./form-directivas.component.css']
})
export class FormDirectivasComponent {

  estadoSeccion = 0;


  alumnos = [
    {nombre: "Nicolas Alexis"},
    {nombre: "Julian"},
    {nombre: "Rolando Diego"},
    {nombre: "Jose Carlos"},
  ];

  cursos = [
    {nombre: "Desarrollo avanzado de aplicaciones 2"},
    {nombre: "Desarrollo de aplicaciones moviles 2"},
    {nombre: "Desarrollo de servicios web 2"},
  ];

  fechaActual = new Date();
  precio = 150;

  mostrarAlumno(): void{
    this.estadoSeccion = 0;
  }

  mostrarCursos(): void{
    this.estadoSeccion = 1;
  }


}
