import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDirectivasComponent } from './form-directivas.component';

describe('FormDirectivasComponent', () => {
  let component: FormDirectivasComponent;
  let fixture: ComponentFixture<FormDirectivasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormDirectivasComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormDirectivasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
