import { Component } from '@angular/core';

@Component({
  selector: 'app-form-calculo-imc',
  templateUrl: './form-calculo-imc.component.html',
  styleUrls: ['./form-calculo-imc.component.css']
})
export class FormCalculoImcComponent {

  frmImc = {
     peso: null,  
     talla: null, 
     resultado: 0
     }

     fotoUrl = "https://www.idat.edu.pe/sites/default/files/banner-facultades/Banners-carreras_1.jpg"

     mensaje: String = "Mensaje de interpolacion"

     calcularImc(){
      var tallam = Number(this.frmImc.talla) / 100
      this.frmImc.resultado = Number(this.frmImc.peso) / (tallam * tallam)
      this.mensaje = "Su Imc = " + this.frmImc.resultado
     }
}
