import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormOperacionComponent } from './componentes-bs/form-operacion/form-operacion.component';

import { FormListaAlumnosComponent } from './componentes-bs/form-lista-alumnos/form-lista-alumnos.component';
import { FormsModule } from '@angular/forms';
import { FormDirectivasComponent } from './componentes-bs/form-directivas/form-directivas.component';
import { FormRegistroDeNotasComponent } from './componentes-bs/form-registro-de-notas/form-registro-de-notas.component';
import { FormCalculoImcComponent } from './componentes-bs/form-calculo-imc/form-calculo-imc.component';
import { FormRegistroDeDocentesComponent } from './componentes-bs/form-registro-de-docentes/form-registro-de-docentes.component';

@NgModule({
  declarations: [
    AppComponent,
    FormOperacionComponent,
    FormListaAlumnosComponent,
    FormDirectivasComponent,
    FormRegistroDeNotasComponent,
    FormCalculoImcComponent,
    FormRegistroDeDocentesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
