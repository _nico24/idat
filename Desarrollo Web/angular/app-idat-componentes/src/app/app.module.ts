import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormSumaComponent } from './form-suma/form-suma.component';
import { FormRegistroComponent } from './new-components/form-registro/form-registro.component';
import { FormModificarComponent } from './new-components/form-modificar/form-modificar.component';
import { FormRestaComponent } from './form-resta/form-resta.component';

@NgModule({
  declarations: [
    AppComponent,
    FormSumaComponent,
    FormRegistroComponent,
    FormModificarComponent,
    FormRestaComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
