import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormRestaComponent } from './form-resta.component';

describe('FormRestaComponent', () => {
  let component: FormRestaComponent;
  let fixture: ComponentFixture<FormRestaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormRestaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormRestaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
