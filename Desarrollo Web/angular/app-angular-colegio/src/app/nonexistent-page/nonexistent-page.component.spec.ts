import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NonexistentPageComponent } from './nonexistent-page.component';

describe('NonexistentPageComponent', () => {
  let component: NonexistentPageComponent;
  let fixture: ComponentFixture<NonexistentPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NonexistentPageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NonexistentPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
