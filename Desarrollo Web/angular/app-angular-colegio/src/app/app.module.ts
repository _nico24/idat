import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { ControllersComponent } from './controllers/controllers.component';
import { MainMenuComponent } from './controllers/main-menu/main-menu.component';
import { AttendanceListComponent } from './controllers/attendance/attendance-list/attendance-list.component';
import { NotesListComponent } from './controllers/notes/notes-list/notes-list.component';
import { PaymentInquiryListComponent } from './controllers/payment-inquiry/payment-inquiry-list/payment-inquiry-list.component';
import { QuotesListComponent } from './controllers/quotes/quotes-list/quotes-list.component';
import { RatingsListComponent } from './controllers/ratings/ratings-list/ratings-list.component';
import { StudentsListComponent } from './controllers/students/students-list/students-list.component';
import { TeachersListComponent } from './controllers/teachers/teachers-list/teachers-list.component';
import { WorkshopsListComponent } from './controllers/workshops/workshops-list/workshops-list.component';
import { NonexistentPageComponent } from './nonexistent-page/nonexistent-page.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    
    LoginComponent,
    ControllersComponent,
    MainMenuComponent,
    AttendanceListComponent,
    NotesListComponent,
    PaymentInquiryListComponent,
    QuotesListComponent,
    RatingsListComponent,
    StudentsListComponent,
    TeachersListComponent,
    WorkshopsListComponent,
    NonexistentPageComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path:"login", component: LoginComponent},
      {path: "controllers", component: ControllersComponent,
    children:[
      {path: "attendance", component: AttendanceListComponent},
      {path: "main-menu", component: MainMenuComponent},
      {path: "notes", component: NotesListComponent},
      {path: "payment-inquiry", component: PaymentInquiryListComponent},
      {path: "quotes", component: QuotesListComponent},
      {path: "ratings", component: RatingsListComponent},
      {path: "students", component: StudentsListComponent},
      {path: "teachers", component: TeachersListComponent},
      {path: "workshops", component: WorkshopsListComponent}
    ]},

    
    {path: "", redirectTo: "login", pathMatch: "full"},
    {path: "**", component: NonexistentPageComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
