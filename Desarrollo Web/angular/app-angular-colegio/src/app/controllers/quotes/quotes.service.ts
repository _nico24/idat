import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Quotes } from './quotes';

@Injectable({
  providedIn: 'root'
})
export class QuotesService {

  constructor(private http: HttpClient) { }

  quoteslist(): Observable<Quotes[]>{
    return this.http.get<Quotes[]>("https://6438252ef3a0c40814abf320.mockapi.io/api/v1/docente/Citas")
  }

}
