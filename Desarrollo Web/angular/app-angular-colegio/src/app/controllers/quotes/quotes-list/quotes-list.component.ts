import { Component, OnInit } from '@angular/core';
import { Quotes } from '../quotes';
import { QuotesService } from '../quotes.service';

@Component({
  selector: 'app-quotes-list',
  templateUrl: './quotes-list.component.html',
  styleUrls: ['./quotes-list.component.css']
})
export class QuotesListComponent implements OnInit{

  Quotes: Quotes[] = []

  citas: Quotes[] = [];

  fecha: string = '';
  horaInicio: string = '';
  horaFin: string = '';
  estado: string = 'Pendiente';
  tipo: string = 'Presencial';
  asunto: string = '';
  desarrollo: string = '';
  compromiso: string = '';
  comentarioInterno: string = '';


  constructor(private quotesService: QuotesService){}

  ngOnInit(): void {
   this.quotesService.quoteslist().subscribe((data) => {this.Quotes = data})
  }


  registrarCita(citaForm: any) {
    if (citaForm.invalid) {
      return;
    }
}

eliminarCita(quotes: Quotes) {
  const index = this.Quotes.indexOf(quotes);
  if (index !== -1) {
  this.Quotes.splice(index, 1);
  }
  }

}


