export interface Quotes {
    id: string,
    fecha: string,
    hora_inicio: string,
    hora_fin: string,
    estado: string,
    tipo: string,
    asunto: string,
    desarrollo: string,
    compromiso: string,
    comentario: string
}
