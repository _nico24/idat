import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Notes } from './notes';

@Injectable({
  providedIn: 'root'
})
export class NotesService {
  

  constructor(private http: HttpClient) { }

  noteslist(): Observable<Notes[]>{
    return this.http.get<Notes[]>("https://64377cea894c9029e8befd6e.mockapi.io/api/v1/Notas")
  }

  
}
