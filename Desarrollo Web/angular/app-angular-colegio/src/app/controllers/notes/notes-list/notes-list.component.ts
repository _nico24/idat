import { Component, OnInit } from '@angular/core';
import { Notes } from '../notes';
import { NotesService } from '../notes.service';
@Component({
  selector: 'app-notes-list',
  templateUrl: './notes-list.component.html',
  styleUrls: ['./notes-list.component.css']
})
export class NotesListComponent  implements OnInit{
  fotoUrl = "https://thumbs.dreamstime.com/b/lista-de-control-de-relleno-de-la-mano-en-el-tablero-84320804.jpg"

  fotoUrl1 = "https://media.gettyimages.com/id/628133336/es/vector/cuestionario.jpg?s=612x612&w=gi&k=20&c=BfFFkmAP6fHZjm07GoRpcKWfXMmqX95bHa_JCBxiTVs="

  Notes: Notes[] = []

  constructor(private notesService: NotesService){}

  ngOnInit(): void {
    this.notesService.noteslist().subscribe((data) => {this.Notes = data})
  }

  eliminarNota(notes: Notes) {
    const index = this.Notes.indexOf(notes);
    if (index !== -1) {
    this.Notes.splice(index, 1);
    }
    }




}
