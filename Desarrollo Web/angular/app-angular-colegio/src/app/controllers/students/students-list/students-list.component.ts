import { Component, OnInit } from '@angular/core';
import { Students } from '../students';
import { StudentsService } from '../students.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.component.html',
  styleUrls: ['./students-list.component.css']
})
export class StudentsListComponent implements OnInit{

  Students: Students[] = []
  // datasaved=false;
  studentForm!: FormGroup;
  BookitoUpdate=null;

  constructor(private formbuilder:FormBuilder, private studentsService: StudentsService){}

  ngOnInit(): void {
    this.studentForm=this.formbuilder.group({      
      nombre: [' ',[Validators.required]],
      apellido: [' ',[Validators.required]],
      grado: [' ',[Validators.required]],
      seccion: [' ',[Validators.required]],
      edad: [' ',[Validators.required]],
      email: [' ',[Validators.required]]
    });
    this.getAllStudents();
  }

  onFormSubmit(){
    //! obtener informacion del formulario
    let student: Students =this.studentForm.value;
    student.id = "";
    //! utlizar metodo crear
    this.createStudent(student);
    //! limpiar formulario
    this.studentForm.reset();
  }

  private getAllStudents(): void {
    this.studentsService.studentslist().subscribe((data) => {this.Students = data})
  }

  eliminarEstudiante(id: string) {
    //! verificar si el id es mayor a 0
    if (Number(id) <= 0) return;
    //! eliminar estudiante
    this.studentsService.deleteStudent(id).subscribe((data)=> {
      //! si se elimina obtener todos los estudiantes
      this.getAllStudents();
    })
  }

  private createStudent(student: Students): void {
    //! utilizar servicio crear
    this.studentsService.createStudent(student).subscribe((data)=> {
      //! si se elimina obtener todos los estudiantes
      this.getAllStudents();
    });
  }
}
