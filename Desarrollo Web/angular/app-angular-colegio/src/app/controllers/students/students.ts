export interface Students {
    id: string,
    nombre: string,
    apellido: string,
    grado: string,
    seccion: string,
    edad: string,
    email: string
}
