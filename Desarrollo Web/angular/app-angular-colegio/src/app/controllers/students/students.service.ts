import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Students } from './students';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  constructor(private http: HttpClient) { }

  studentslist(): Observable<Students[]>{
    return this.http.get<Students[]>("https://643893294660f26eb19f3e9a.mockapi.io/api/v1/alumno/alumno")
  }

  deleteStudent(id: string): Observable<Students[]>{
    return this.http.delete<Students[]>(`https://643893294660f26eb19f3e9a.mockapi.io/api/v1/alumno/alumno/${id}`)
  }

  createStudent(student: Students): Observable<Students>{
    let httpheaders = new HttpHeaders()
    .set('Content-type','application/Json');
    let options={
      headers:httpheaders
    };
    return this.http.post<Students>("https://643893294660f26eb19f3e9a.mockapi.io/api/v1/alumno/alumno",student,options);
  }
}
