import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentInquiryListComponent } from './payment-inquiry-list.component';

describe('PaymentInquiryListComponent', () => {
  let component: PaymentInquiryListComponent;
  let fixture: ComponentFixture<PaymentInquiryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentInquiryListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PaymentInquiryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
