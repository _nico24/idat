import { Component, OnInit } from '@angular/core';
import { PaymentInquiryService } from '../payment-inquiry.service';
import { PaymentInquiry } from '../payment-inquiry';

@Component({
  selector: 'app-payment-inquiry-list',
  templateUrl: './payment-inquiry-list.component.html',
  styleUrls: ['./payment-inquiry-list.component.css']
})
export class PaymentInquiryListComponent  implements OnInit{

  paymentinquiry: PaymentInquiry[] = []

  consulta: PaymentInquiry[] = []

  constructor(private paymentinquiryService: PaymentInquiryService){}

  ngOnInit(): void {
    this.paymentinquiryService.paymentinquirylist().subscribe((data) => {this.paymentinquiry = data})
  }

  eliminarConsulta(consulta: PaymentInquiry) {
    const index = this.paymentinquiry.indexOf(consulta);
    if (index !== -1) {
    this.paymentinquiry.splice(index, 1);
    }
    }
  

}
