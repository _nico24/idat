export interface PaymentInquiry {
    id: string,
    dni: string,
    nombre: string,
    apellido: string,
    fecha: string,
    monto: string,
    estado: string,
}
