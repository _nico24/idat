import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PaymentInquiry } from './payment-inquiry';

@Injectable({
  providedIn: 'root'
})
export class PaymentInquiryService {

  constructor(private http: HttpClient) { }

  paymentinquirylist(): Observable<PaymentInquiry[]>{
    return this.http.get<PaymentInquiry[]>("https://64387fcf1b9a7dd5c952639e.mockapi.io/api/v1/consulta/consulta")
  }

}
