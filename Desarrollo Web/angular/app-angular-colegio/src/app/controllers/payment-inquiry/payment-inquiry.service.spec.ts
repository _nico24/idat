import { TestBed } from '@angular/core/testing';

import { PaymentInquiryService } from './payment-inquiry.service';

describe('PaymentInquiryService', () => {
  let service: PaymentInquiryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PaymentInquiryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
