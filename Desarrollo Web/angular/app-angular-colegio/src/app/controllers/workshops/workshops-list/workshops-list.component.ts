import { Component, OnInit } from '@angular/core';
import { Workshops } from '../workshops';
import { WorkshopsService } from '../workshops.service';

@Component({
  selector: 'app-workshops-list',
  templateUrl: './workshops-list.component.html',
  styleUrls: ['./workshops-list.component.css']
})
export class WorkshopsListComponent implements OnInit{

  Workshops: Workshops[] = []

  constructor(private workshopsService: WorkshopsService){}

  ngOnInit(): void {
    this.workshopsService.workshopslist().subscribe((data) => {this.Workshops = data})
  }

  eliminarTaller(workshops: Workshops) {
    const index = this.Workshops.indexOf(workshops);
    if (index !== -1) {
    this.Workshops.splice(index, 1);
    }
    }
}
