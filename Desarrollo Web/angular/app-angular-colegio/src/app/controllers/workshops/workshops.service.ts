import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Workshops } from './workshops';

@Injectable({
  providedIn: 'root'
})
export class WorkshopsService {

  constructor(private http: HttpClient) { }

  workshopslist(): Observable<Workshops[]>{
    return this.http.get<Workshops[]>("https://64387fcf1b9a7dd5c952639e.mockapi.io/api/v1/consulta/taller")
  }
}
