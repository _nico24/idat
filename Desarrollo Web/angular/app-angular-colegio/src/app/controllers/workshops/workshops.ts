export interface Workshops {
    id: string,
    taller: string,
    descripcion: string,
    horario: string,
    ubicacion: string
}
