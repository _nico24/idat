import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form-menu-principal',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.css']
})
export class MainMenuComponent {
  constructor(private router: Router){

  }
  onSubmit(): void{
    this.router.navigate(["main-menu"]);
  }
}
