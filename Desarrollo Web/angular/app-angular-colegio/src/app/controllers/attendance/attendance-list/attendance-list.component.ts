import { Component, OnInit } from '@angular/core';
import { Attendance } from '../attendance';
import { AttendanceService } from "../attendance.service"
@Component({
  selector: 'app-attendance-list',
  templateUrl: './attendance-list.component.html',
  styleUrls: ['./attendance-list.component.css']
})
export class AttendanceListComponent implements OnInit{


    Attendance: Attendance[] = []


    constructor(private attendanceService: AttendanceService){
    }

    ngOnInit(): void {
     this.attendanceService.attendancelist().subscribe((data) => {
      this.Attendance = data
     })
    }

    eliminarAsistencia(attendance: Attendance) {
      const index = this.Attendance.indexOf(attendance);
      if (index !== -1) {
      this.Attendance.splice(index, 1);
      }
      }
}
