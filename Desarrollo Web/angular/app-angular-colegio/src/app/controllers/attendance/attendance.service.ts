import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Attendance } from './attendance';

@Injectable({
  providedIn: 'root'
})
export class AttendanceService {

  constructor(private http: HttpClient) { }

  attendancelist(): Observable<Attendance[]>{
    return this.http.get<Attendance[]>("https://64377cea894c9029e8befd6e.mockapi.io/api/v1/Asistencia")
  }

}
