export interface Attendance {
    id: string,
    nombres: string,
    apellidos: string,
    seccion: string,
    turno: string,
    lunes: string,
    martes: string,
    miercoles: string,
    jueves: string,
    viernes: string,
}
