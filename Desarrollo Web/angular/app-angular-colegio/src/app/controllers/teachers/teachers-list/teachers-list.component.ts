import { Component, OnInit } from '@angular/core';
import { TeachersService } from '../teachers.service';
import { Teachers } from '../teachers';

@Component({
  selector: 'app-teachers-list',
  templateUrl: './teachers-list.component.html',
  styleUrls: ['./teachers-list.component.css']
})
export class TeachersListComponent implements OnInit{

  Teachers: Teachers[] = []

  docente: Teachers = {
    nombre: '',
    apellido: '',
    condicion: '',
    correo: '',
    telefono: '',
    curso: '',
    id: ''
  };

  docentesRegistrados: Teachers[] = [];
  docenteEditando: Teachers | undefined;
  editando: boolean = false;
  
  registrarDocente() {
    this.docentesRegistrados.push(this.docente);
    this.docente = {
    id: '',
    nombre: '',
    apellido: '',
    condicion: '',
    correo: '',
    telefono: '',
    curso: ''
    };
    }


  constructor(private teachersService: TeachersService){}

  ngOnInit(): void {
    this.teachersService.teacherslist().subscribe((data) =>{this.Teachers = data})
  }

  eliminarDocente(teachers: Teachers) {
    const index = this.Teachers.indexOf(teachers);
    if (index !== -1) {
    this.Teachers.splice(index, 1);
    }
    }
}

