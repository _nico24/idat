export interface Teachers {
    id: string,
    nombre: string,
    apellido: string,
    condicion: string,
    correo: string,
    telefono: string,
    curso: string,
}
