import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Teachers } from './teachers';


@Injectable({
  providedIn: 'root'
})
export class TeachersService {

  constructor(private http: HttpClient) { }


  teacherslist(): Observable<Teachers[]>{
    return this.http.get<Teachers[]>("https://6438252ef3a0c40814abf320.mockapi.io/api/v1/docente/Docentes")
  }
}
