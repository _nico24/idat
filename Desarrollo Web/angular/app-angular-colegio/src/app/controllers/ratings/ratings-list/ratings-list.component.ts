import { Component, OnInit } from '@angular/core';
import { Ratings } from '../ratings';
import { RatingsService } from '../ratings.service';

@Component({
  selector: 'app-ratings-list',
  templateUrl: './ratings-list.component.html',
  styleUrls: ['./ratings-list.component.css']
})
export class RatingsListComponent implements OnInit{

  Ratings: Ratings[] = []

  constructor(private ratingsService: RatingsService){}
  ngOnInit(): void {
    this.ratingsService.ratingslist().subscribe((data)=>{this.Ratings = data})
  }

  eliminarlibreta(ratings: Ratings) {
    const index = this.Ratings.indexOf(ratings);
    if (index !== -1) {
    this.Ratings.splice(index, 1);
    }
    }

}
