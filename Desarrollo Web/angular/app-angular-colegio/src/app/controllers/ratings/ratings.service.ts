import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Ratings } from './ratings';

@Injectable({
  providedIn: 'root'
})
export class RatingsService {

  constructor(private http: HttpClient) { }

  ratingslist(): Observable<Ratings[]>{
    return this.http.get<Ratings[]>("https://643893294660f26eb19f3e9a.mockapi.io/api/v1/alumno/libreta")
  }
}
