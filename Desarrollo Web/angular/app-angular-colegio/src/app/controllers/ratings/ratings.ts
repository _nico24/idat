export interface Ratings {
    id: string,
    nombre: string,
    apellido: string,
    edad: string,
    grado: string,
    examen_1: string,
    examen_2: string,
    examen_3: string,
    examen_4: string
}
