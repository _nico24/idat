import { Component } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-controllers',
  templateUrl: './controllers.component.html',
  styleUrls: ['./controllers.component.css']
})
export class ControllersComponent {
  constructor(private route: ActivatedRoute, private router: Router){

  }
  onNavigateToMainMenu():void{
    this.router.navigate(["main-menu"], {relativeTo: this.route})
  }

  onNavigateToNotes(): void{
    this.router.navigate(["notes"], {relativeTo: this.route})
  }

  onNavigateToPayment_Inquiry(): void{
    this.router.navigate(["payment-inquiry"], {relativeTo: this.route})
  }

  onNavigateToAttendance(): void{
    this.router.navigate(["attendance"], {relativeTo: this.route})
  }

  onNavigateToQuotes(): void{
    this.router.navigate(["quotes"], {relativeTo: this.route})
  }

  onNavigateToRatings(): void{
    this.router.navigate(["ratings"], {relativeTo: this.route})
  }

  onNavigateToStudents(): void{
    this.router.navigate(["students"], {relativeTo: this.route})
  }

  onNavigateToTeachers(): void{
    this.router.navigate(["teachers"], {relativeTo: this.route})
  }

  onNavigateToWorkshops(): void{
    this.router.navigate(["workshops"], {relativeTo: this.route})
  }
}
