import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'; // Importa FormsModule

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormListaAsistenciaComponent } from './components/form-consulta-asistencia-alumnos/form-consulta-asistencia-alumnos.component';
import { FormConsultaNotasComponent } from './components/form-consulta-notas/form-consulta-notas.component';
import { FormRegistroDocentesComponent } from './components/form-registro-docentes/form-registro-docentes.component';
import { FormRegistroCitasAlumnosComponent } from './components/form-registro-citas-alumnos/form-registro-citas-alumnos.component';
import { FormMenuPrincipalComponent } from './components/form-menu-principal/form-menu-principal.component';

@NgModule({
  declarations: [
    AppComponent,
    FormListaAsistenciaComponent,
    FormConsultaNotasComponent,
    FormRegistroDocentesComponent,
    FormRegistroCitasAlumnosComponent,
    FormMenuPrincipalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
