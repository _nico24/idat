import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormConsultaNotasComponent } from './components/form-consulta-notas/form-consulta-notas.component';
import { FormListaAsistenciaComponent } from './components/form-consulta-asistencia-alumnos/form-consulta-asistencia-alumnos.component';
import { FormRegistroCitasAlumnosComponent } from './components/form-registro-citas-alumnos/form-registro-citas-alumnos.component';
import { FormRegistroDocentesComponent } from './components/form-registro-docentes/form-registro-docentes.component';
import {FormMenuPrincipalComponent} from "./components/form-menu-principal/form-menu-principal.component";
const routes: Routes = [
  { path: 'notas', component: FormConsultaNotasComponent },
  { path: 'asistencia', component: FormListaAsistenciaComponent },
  { path: 'citas', component: FormRegistroCitasAlumnosComponent },
  { path: 'docente', component: FormRegistroDocentesComponent },
  { path: 'inicio', component: FormMenuPrincipalComponent },
];

// export const routing = RouterModule.forRoot(routes);

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
