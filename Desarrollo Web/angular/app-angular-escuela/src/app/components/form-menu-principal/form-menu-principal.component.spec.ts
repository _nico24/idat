import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormMenuPrincipalComponent } from './form-menu-principal.component';

describe('FormMenuPrincipalComponent', () => {
  let component: FormMenuPrincipalComponent;
  let fixture: ComponentFixture<FormMenuPrincipalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormMenuPrincipalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormMenuPrincipalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
