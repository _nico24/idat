import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormRegistroDocentesComponent } from './form-registro-docentes.component';

describe('FormRegistroDocentesComponent', () => {
  let component: FormRegistroDocentesComponent;
  let fixture: ComponentFixture<FormRegistroDocentesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormRegistroDocentesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormRegistroDocentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
