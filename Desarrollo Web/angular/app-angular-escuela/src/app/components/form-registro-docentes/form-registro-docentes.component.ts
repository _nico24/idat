import { Component } from '@angular/core';

interface Docente {
dni: string;
nombre: string;
apellidos: string;
condicion: string;
correo: string;
telefono: string;
curso: string;
}

@Component({
selector: 'app-form-registro-docentes',
templateUrl: './form-registro-docentes.component.html',
styleUrls: ['./form-registro-docentes.component.css']
})
export class FormRegistroDocentesComponent {
docente: Docente = {
dni: '',
nombre: '',
apellidos: '',
condicion: '',
correo: '',
telefono: '',
curso: ''
};

docentesRegistrados: Docente[] = [];

docenteEditando: Docente | undefined;
editando: boolean = false;

registrarDocente() {
this.docentesRegistrados.push(this.docente);
this.docente = {
dni: '',
nombre: '',
apellidos: '',
condicion: '',
correo: '',
telefono: '',
curso: ''
};
}

eliminarDocente(docente: Docente) {
const index = this.docentesRegistrados.indexOf(docente);
if (index !== -1) {
this.docentesRegistrados.splice(index, 1);
}
}
}