import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormConsultaNotasComponent } from './form-consulta-notas.component';

describe('ListanotasComponent', () => {
  let component: FormConsultaNotasComponent;
  let fixture: ComponentFixture<FormConsultaNotasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormConsultaNotasComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormConsultaNotasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
});
});