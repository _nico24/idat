import { Component } from '@angular/core';

@Component({
  selector: 'app-listanotas',
  templateUrl: './form-consulta-notas.component.html',
  styleUrls: ['./form-consulta-notas.component.css']
})
export class FormConsultaNotasComponent {

  lstAlumno = [
    {id: 202201, nombre: "Marlon", Apellido: "Zapata", estado: "Aprobado"},
    {id: 202302, nombre: "Samuel", Apellido: "Sorote", estado: "Aprobado"},
    {id: 202303, nombre: "Danitza", Apellido: "Sandoval", estado: "Aprobado"},
    {id: 202304, nombre: "Milagros", Apellido: "Soto", estado: "Aprobado"},
    {id: 202305, nombre: "Jairo", Apellido: "Lopez", estado: "Desaprobado"},
    {id: 202306, nombre: "Susan", Apellido: "Josa", estado: "Aprobado"},
  ]

  fotoUrl = "https://thumbs.dreamstime.com/b/lista-de-control-de-relleno-de-la-mano-en-el-tablero-84320804.jpg"

  fotoUrl1 = "https://media.gettyimages.com/id/628133336/es/vector/cuestionario.jpg?s=612x612&w=gi&k=20&c=BfFFkmAP6fHZjm07GoRpcKWfXMmqX95bHa_JCBxiTVs="

}