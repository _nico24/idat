import { Component } from '@angular/core';

interface Cita {
  fecha: string;
  horaInicio: string;
  horaFin: string;
  estado: string;
  tipo: string;
  asunto: string;
  desarrollo: string;
  compromiso: string;
  comentarioInterno?: string;
}

@Component({
  selector: 'app-form-registro-citas-alumnos',
  templateUrl: './form-registro-citas-alumnos.component.html',
  styleUrls: ['./form-registro-citas-alumnos.component.css']
})
export class FormRegistroCitasAlumnosComponent {
  citas: Cita[] = [];

  fecha: string = '';
  horaInicio: string = '';
  horaFin: string = '';
  estado: string = 'Pendiente';
  tipo: string = 'Presencial';
  asunto: string = '';
  desarrollo: string = '';
  compromiso: string = '';
  comentarioInterno: string = '';

  registrarCita(citaForm: any) {
    if (citaForm.invalid) {
      return;
    }

    const nuevaCita: Cita = {
      fecha: this.fecha,
      horaInicio: this.horaInicio,
      horaFin: this.horaFin,
      estado: this.estado,
      tipo: this.tipo,
      asunto: this.asunto,
      desarrollo: this.desarrollo,
      compromiso: this.compromiso,
      comentarioInterno: this.comentarioInterno
    };
    
    this.citas.push(nuevaCita);
    
    this.limpiarFormulario(citaForm);
  }

  limpiarFormulario(citaForm: any) {
  citaForm.resetForm();
  this.fecha = '';
  this.horaInicio = '';
  this.horaFin = '';
  this.estado = 'Pendiente';
  this.tipo = 'Presencial';
  this.asunto = '';
  this.desarrollo = '';
  this.compromiso = '';
  this.comentarioInterno = '';
  }
  }
