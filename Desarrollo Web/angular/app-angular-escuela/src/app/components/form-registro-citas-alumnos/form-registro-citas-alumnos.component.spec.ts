import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormRegistroCitasAlumnosComponent } from './form-registro-citas-alumnos.component';

describe('FormRegistroCitasAlumnosComponent', () => {
  let component: FormRegistroCitasAlumnosComponent;
  let fixture: ComponentFixture<FormRegistroCitasAlumnosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormRegistroCitasAlumnosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormRegistroCitasAlumnosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
