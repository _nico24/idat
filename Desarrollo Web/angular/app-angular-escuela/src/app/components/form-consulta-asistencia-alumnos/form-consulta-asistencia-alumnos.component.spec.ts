import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormConsultaAsistenciaAlumnosComponent } from './form-consulta-asistencia-alumnos.component';

describe('FormConsultaAsistenciaAlumnosComponent', () => {
  let component: FormConsultaAsistenciaAlumnosComponent;
  let fixture: ComponentFixture<FormConsultaAsistenciaAlumnosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormConsultaAsistenciaAlumnosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormConsultaAsistenciaAlumnosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
