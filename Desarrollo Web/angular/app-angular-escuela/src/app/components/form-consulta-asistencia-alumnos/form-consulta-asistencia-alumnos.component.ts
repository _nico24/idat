import { Component } from '@angular/core';

@Component({
  selector: 'app-form-consulta-asistencia-alumnos',
  templateUrl: './form-consulta-asistencia-alumnos.component.html',
  styleUrls: ['./form-consulta-asistencia-alumnos.component.css']
})
export class FormListaAsistenciaComponent {

  lstAlumno = [
    {id: 4544432, nombre: "Marlon", Apellido: "zapata", dia1: "A",dia2: "A",dia3: "F",dia4: "",dia5: "",dia6: "",dia7: "",dia8: "",dia9: ""},
    {id: 5658754, nombre: "Samuel", Apellido: "sorote", dia1: "A",dia2: "A",dia3: "A",dia4: "",dia5: "",dia6: "",dia7: "",dia8: "",dia9: ""},
    {id: 6749465, nombre: "Danitza", Apellido: "sabdoval", dia1: "F",dia2: "A",dia3: "A",dia4: "",dia5: "",dia6: "",dia7: "",dia8: "",dia9: ""},
    {id: 9545654, nombre: "Milagros", Apellido: "soto", dia1: "A",dia2: "A",dia3: "A",dia4: "",dia5: "",dia6: "",dia7: "",dia8: "",dia9: ""},
    {id: 5475556, nombre: "Jairo", Apellido: "lopez", dia1: "A",dia2: "F",dia3: "A",dia4: "",dia5: "",dia6: "",dia7: "",dia8: "",dia9: ""},
    {id: 5456554, nombre: "Susan", Apellido: "josa", dia1: "A",dia2: "A",dia3: "A",dia4: "",dia5: "",dia6: "",dia7: "",dia8: "",dia9: ""},
  ]

  fotoUrl = "https://thumbs.dreamstime.com/b/lista-de-control-de-relleno-de-la-mano-en-el-tablero-84320804.jpg"

  fotoUrl1 = "https://media.gettyimages.com/id/628133336/es/vector/cuestionario.jpg?s=612x612&w=gi&k=20&c=BfFFkmAP6fHZjm07GoRpcKWfXMmqX95bHa_JCBxiTVs="

}
