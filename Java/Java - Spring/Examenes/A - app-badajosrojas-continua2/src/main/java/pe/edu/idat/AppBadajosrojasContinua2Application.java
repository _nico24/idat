package pe.edu.idat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppBadajosrojasContinua2Application {

	public static void main(String[] args) {
		SpringApplication.run(AppBadajosrojasContinua2Application.class, args);
	}

}
