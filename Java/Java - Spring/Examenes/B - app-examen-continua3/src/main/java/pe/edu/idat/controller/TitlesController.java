package pe.edu.idat.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import pe.edu.idat.model.bd.Titles;
import pe.edu.idat.service.TitlesService;

@Controller
@RequestMapping("/title")
public class TitlesController {

    @Autowired
    private TitlesService titleService;

    @GetMapping("/list")
    public String listTitles(Model model) {
        List<Titles> titles = titleService.findAllTitles();
        model.addAttribute("titles", titles);
        return "title/list";
    }

    @GetMapping("/form")
    public String showTitleForm(Model model) {
        model.addAttribute("title", new Titles());
        return "title/form";
    }

    @PostMapping("/save")
    public String saveTitle(Titles title) {
        titleService.saveTitle(title);
        return "redirect:/title/list";
    }

    @GetMapping("/edit/{titleId}")
    public String showEditForm(@PathVariable("titleId") Integer titleId, Model model) {
        Optional<Titles> title = titleService.findTitleById(titleId);
        if (title.isPresent()) {
            model.addAttribute("title", title.get());
        }
        return "title/form";
    }

    @GetMapping("/delete/{titleId}")
    public String deleteTitle(@PathVariable("titleId") Integer titleId) {
        titleService.deleteTitle(titleId);
        return "redirect:/title/list";
    }
}
