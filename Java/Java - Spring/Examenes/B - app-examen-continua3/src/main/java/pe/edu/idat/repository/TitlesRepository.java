package pe.edu.idat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import jakarta.transaction.Transactional;
import pe.edu.idat.model.bd.Titles;

@Repository
public interface TitlesRepository extends JpaRepository<Titles, Integer> {

    @Transactional
    @Modifying
    @Query(
            value = "{call sp_MantRegistrarTitle(:title,:type,:price,:notes)}",
            nativeQuery = true)
    void registrarTitle(@Param("title") String title, 
            @Param("type") String type, 
            @Param("price") Double price, 
            @Param("notes") String notes);
    
}
