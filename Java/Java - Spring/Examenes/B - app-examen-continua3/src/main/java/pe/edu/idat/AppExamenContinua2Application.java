package pe.edu.idat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppExamenContinua2Application {

	public static void main(String[] args) {
		SpringApplication.run(AppExamenContinua2Application.class, args);
	}

}
