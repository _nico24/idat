package pe.edu.idat.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class PublishersResponse {
    private Integer pubId;
    private String pubName;
    private String city;
    private String country;
}
