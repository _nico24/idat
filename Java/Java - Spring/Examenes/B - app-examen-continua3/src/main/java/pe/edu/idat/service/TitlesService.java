package pe.edu.idat.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.model.bd.Titles;
import pe.edu.idat.repository.TitlesRepository;

@Service
public class TitlesService {

    @Autowired
    private TitlesRepository titlesRepository;

    public List<Titles> findAllTitles() {
        return titlesRepository.findAll();
    }

    public Optional<Titles> findTitleById(Integer titleId) {
        return titlesRepository.findById(titleId);
    }

    public void saveTitle(Titles title) {
        titlesRepository.save(title);
    }

    public void deleteTitle(Integer titleId) {
        titlesRepository.deleteById(titleId);
    }
}
