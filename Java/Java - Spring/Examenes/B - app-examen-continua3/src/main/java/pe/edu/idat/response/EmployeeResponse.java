package pe.edu.idat.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class EmployeeResponse {
    private Integer empId;
    private String firstName;
    private String lastName;
    private Integer middleInitial;
}
