package pe.edu.idat.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pe.edu.idat.model.bd.Employee;
import pe.edu.idat.service.EmployeeService;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/list")
    public String listEmployees(Model model) {
        List<Employee> employees = employeeService.findAllEmployees();
        model.addAttribute("employees", employees);
        return "employee/list";
    }

    @GetMapping("/form")
    public String showEmployeeForm(Model model) {
        model.addAttribute("employee", new Employee());
        return "employee/form";
    }

    @PostMapping("/save")
    public String saveEmployee(Employee employee) {
        employeeService.saveEmployee(employee);
        return "redirect:/employee/list";
    }

    @GetMapping("/edit/{empId}")
    public String showEditForm(@PathVariable("empId") Integer empId, Model model) {
        Optional<Employee> employee = employeeService.findEmployeeById(empId);
        if (employee.isPresent()) {
            model.addAttribute("employee", employee.get());
        }
        return "employee/form";
    }

    @GetMapping("/delete/{empId}")
    public String deleteEmployee(@PathVariable("empId") Integer empId) {
        employeeService.deleteEmployee(empId);
        return "redirect:/employee/list";
    }
}
