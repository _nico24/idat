package pe.edu.idat.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.model.bd.Publishers;
import pe.edu.idat.repository.PublishersRepository;

@Service
public class PublishersService {

    @Autowired
    private PublishersRepository publisherRepository;

    public List<Publishers> findAllPublishers() {
        return publisherRepository.findAll();
    }

    public Optional<Publishers> findPublisherById(Integer pubId) {
        return publisherRepository.findById(pubId);
    }

    public void savePublisher(Publishers publisher) {
        publisherRepository.save(publisher);
    }

    public void deletePublisher(Integer pubId) {
        publisherRepository.deleteById(pubId);
    }
}
