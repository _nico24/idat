package pe.edu.idat.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pe.edu.idat.model.bd.Stores;
import pe.edu.idat.service.StoresService;

@Controller
@RequestMapping("/store")
public class StoresController {

    @Autowired
    private StoresService storeService;

    @GetMapping("/list")
    public String listStores(Model model) {
        List<Stores> stores = storeService.findAllStores();
        model.addAttribute("stores", stores);
        return "store/list";
    }

    @GetMapping("/form")
    public String showStoreForm(Model model) {
        model.addAttribute("store", new Stores());
        return "store/form";
    }

    @PostMapping("/save")
    public String saveStore(Stores store) {
        storeService.saveStore(store);
        return "redirect:/store/list";
    }

    @GetMapping("/edit/{storId}")
    public String showEditForm(@PathVariable("storId") Integer storId, Model model) {
        Optional<Stores> store = storeService.findStoreById(storId);
        if (store.isPresent()) {
            model.addAttribute("store", store.get());
        }
        return "store/form";
    }

    @GetMapping("/delete/{storId}")
    public String deleteStore(@PathVariable("storId") Integer storId) {
        storeService.deleteStore(storId);
        return "redirect:/store/list";
    }
}
