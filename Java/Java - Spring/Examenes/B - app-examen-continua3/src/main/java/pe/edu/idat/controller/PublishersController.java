package pe.edu.idat.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pe.edu.idat.model.bd.Publishers;
import pe.edu.idat.service.PublishersService;

@Controller
@RequestMapping("/publisher")
public class PublishersController {

    @Autowired
    private PublishersService publisherService;

    @GetMapping("/list")
    public String listPublishers(Model model) {
        List<Publishers> publishers = publisherService.findAllPublishers();
        model.addAttribute("publishers", publishers);
        return "publisher/list";
    }

    @GetMapping("/form")
    public String showPublisherForm(Model model) {
        model.addAttribute("publisher", new Publishers());
        return "publisher/form";
    }

    @PostMapping("/save")
    public String savePublisher(Publishers publisher) {
        publisherService.savePublisher(publisher);
        return "redirect:/publisher/list";
    }

    @GetMapping("/edit/{pubId}")
    public String showEditForm(@PathVariable("pubId") Integer pubId, Model model) {
        Optional<Publishers> publisher = publisherService.findPublisherById(pubId);
        if (publisher.isPresent()) {
            model.addAttribute("publisher", publisher.get());
        }
        return "publisher/form";
    }

    @GetMapping("/delete/{pubId}")
    public String deletePublisher(@PathVariable("pubId") Integer pubId) {
        publisherService.deletePublisher(pubId);
        return "redirect:/publisher/list";
    }
}
