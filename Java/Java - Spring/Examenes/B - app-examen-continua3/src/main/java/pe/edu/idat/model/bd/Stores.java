package pe.edu.idat.model.bd;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
@Entity
@Table(name = "stores")
public class Stores {

	@Id
	private Integer stor_id;
	
	@Column(name = "stor_name")
	private String stor_name;
	
	@Column(name = "stor_address")
	private String stor_address;
	
	@Column(name = "state")
	private String state;
	
	@Column(name = "zip")
	private String zip;
	
	@OneToMany(mappedBy = "store", cascade = CascadeType.ALL)
	private List<Sales> sales;
	
}
