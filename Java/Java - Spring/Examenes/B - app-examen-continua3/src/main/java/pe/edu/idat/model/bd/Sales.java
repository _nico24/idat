package pe.edu.idat.model.bd;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
@Entity
@Table(name = "sales")
public class Sales {
    
    @Id
    private Integer sale_id;
    
    @Column(name = "qty")
    private Integer qty;
    
    @Column(name = "date")
    private String date;
    
    @ManyToOne
    @JoinColumn(name = "title_id")
    private Titles title;
    
    @ManyToOne
    @JoinColumn(name = "stor_id")
    private Stores store;
    
}
