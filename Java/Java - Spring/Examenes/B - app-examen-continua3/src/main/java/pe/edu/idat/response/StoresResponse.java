package pe.edu.idat.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StoresResponse {
    private int storId;
    private String storName;
    private String storAddress;
    private String state;
    private String zip;
}
