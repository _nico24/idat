package pe.edu.idat.model.bd;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
@Entity
@Table(name = "employee")
public class Employee {

    @Id
    private Integer id;
    
    @Column(name = "fname")
    private String firstName;
    
    @Column(name ="lname")
    private String lastName;
    
    @Column(name = "minit")
    private String middleInitial;
    
}
