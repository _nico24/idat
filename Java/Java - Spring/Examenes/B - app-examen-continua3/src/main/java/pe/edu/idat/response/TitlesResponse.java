package pe.edu.idat.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TitlesResponse {
    private Integer titleId;
    private String title;
    private String type;
    private Double price;
    private String notes;
}
