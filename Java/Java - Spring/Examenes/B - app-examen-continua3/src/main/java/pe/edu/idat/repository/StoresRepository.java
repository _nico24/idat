package pe.edu.idat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import jakarta.transaction.Transactional;
import pe.edu.idat.model.bd.Stores;

@Repository
public interface StoresRepository extends JpaRepository<Stores, Integer> {
    
    @Transactional
    @Modifying
    @Query(
            value = "{call sp_MantRegistrarStore(:storeName, :phoneNumber)}",
            nativeQuery = true)
    void registrarStore(@Param("storeName") String storeName, 
            @Param("phoneNumber") String phoneNumber);
    
}
