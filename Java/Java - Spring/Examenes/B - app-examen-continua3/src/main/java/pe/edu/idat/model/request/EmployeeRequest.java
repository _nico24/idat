package pe.edu.idat.model.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeRequest {

    private Integer empId;
    private String fname;
    private String lname;
    private Integer minit;

}
