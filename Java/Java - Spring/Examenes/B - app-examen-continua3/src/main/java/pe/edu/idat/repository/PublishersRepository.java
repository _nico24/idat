package pe.edu.idat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import jakarta.transaction.Transactional;
import pe.edu.idat.model.bd.Publishers;

@Repository
public interface PublishersRepository extends JpaRepository<Publishers, Integer> {

    @Transactional
    @Modifying
    @Query(
            value = "{call sp_MantRegistrarPublisher(:name,:city,:country)}",
            nativeQuery = true)
    void registrarPublisher(@Param("name") String name, 
            @Param("city")String city, 
            @Param("country")String country);
    
}
