package pe.edu.idat.model.bd;

import java.math.BigDecimal;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
@Entity
@Table(name = "titles")
public class Titles {
    
    @Id
    @Column(name = "title_id")
    private Integer titleId;
    
    @Column(name = "title")
    private String title;
    
    @Column(name = "type")
    private String type;
    
    @Column(name = "price")
    private BigDecimal price;
    
    @Column(name = "notes")
    private String notes;
}
