package pe.edu.idat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import jakarta.transaction.Transactional;
import pe.edu.idat.model.bd.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

    @Transactional
    @Modifying
    @Query(
            value = "{call sp_MantRegistrarEmployee(:name,:surname,:idpos,:process)}",
            nativeQuery = true)
    void registrarEmployee(@Param("name") String name, 
            @Param("surname")String surname, 
            @Param("idpos")String idpos, 
            @Param("process")String process);
    
}
