package pe.edu.idat.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.model.bd.Stores;
import pe.edu.idat.repository.StoresRepository;

@Service
public class StoresService {
	@Autowired
	private StoresRepository storesRepository;

	public List<Stores> findAllStores() {
	    return storesRepository.findAll();
	}

	public Optional<Stores> findStoreById(Integer storeId) {
	    return storesRepository.findById(storeId);
	}

	public void saveStore(Stores store) {
	    storesRepository.save(store);
	}

	public void deleteStore(Integer storeId) {
	    storesRepository.deleteById(storeId);
	}
}
