package pe.edu.idat.model.bd;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "publishers")
public class Publishers {

    @Id
    @Column(name = "pub_id")
    private int pubId;

    @Column(name = "pub_name")
    private String pubName;

    private String city;

    private String country;
}
