package idat.ventas.repositories;

import idat.ventas.models.TransactionModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;

@Repository
public interface TransactionRepository extends CrudRepository<TransactionModel, Long> {
    ArrayList<TransactionModel> findByDate(String date);
}
