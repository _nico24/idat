package idat.ventas.services;

import idat.ventas.interfaces.TransactionInterface;
import idat.ventas.models.TransactionModel;
import idat.ventas.repositories.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Optional;

@Service
public class TransactionService implements TransactionInterface {
    private final TransactionRepository transactionRepository;

    @Autowired
    public TransactionService(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @Override
    public ArrayList<TransactionModel> obtainTransaction() {
        return (ArrayList<TransactionModel>) transactionRepository.findAll();
    }

    @Override
    public TransactionModel saveTransaction(TransactionModel transaction) {
        return transactionRepository.save(transaction);
    }

    @Override
    public Optional<TransactionModel> obtationForId(Long id) {
        return transactionRepository.findById(id);
    }

    @Override
    public ArrayList<TransactionModel> obtainForDate(String date) {
        return transactionRepository.findByDate(date);
    }

    @Override
    public boolean eliminateTransaction(Long id) {
        try {
            transactionRepository.deleteById(id);
            return true;
        } catch (Exception err) {
            return false;
        }
    }

    @Override
    public TransactionModel updateTransaction(Long id, TransactionModel updatedTransaction) throws Exception {
        Optional<TransactionModel> existingTransaction = transactionRepository.findById(id);

        if (existingTransaction.isPresent()) {
            TransactionModel transaction = existingTransaction.get();
            // Actualiza los campos de la transacción existente con los valores del objeto actualizado.
            transaction.setClient(updatedTransaction.getClient());
            transaction.setProduct(updatedTransaction.getProduct());
            transaction.setAmount(updatedTransaction.getAmount());
            transaction.setPrice(updatedTransaction.getPrice());
            transaction.setDate(updatedTransaction.getDate());

            // Guarda la transacción actualizada en la base de datos.
            return transactionRepository.save(transaction);
        } else {
            // Si no se encuentra la transacción con el ID dado, puedes lanzar una excepción o manejar el caso de otra manera.
            throw new Exception("Transaction with ID " + id + " not found");
        }
    }


}
