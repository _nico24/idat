package idat.ventas.controllers;

import idat.ventas.models.TransactionModel;
import idat.ventas.services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/transaction")
public class TransactionController {
    private final TransactionService transactionService;

    @Autowired
    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @GetMapping
    public List<TransactionModel> obtainTransaction(){return transactionService.obtainTransaction();}

    @PostMapping
    public TransactionModel saveTransaction(@RequestBody TransactionModel transaction){
        return transactionService.saveTransaction(transaction);
    }

    @GetMapping(path = "/{nroTransaction}")
    public Optional<TransactionModel> obtainTransactionForNro(@PathVariable("nroTransaction") Long nroTransaction){
        return transactionService.obtationForId(nroTransaction);
    }

    @GetMapping("/query")
    public List<TransactionModel> obtainTransactionForDate(@RequestParam("date") String date){
        return transactionService.obtainForDate(date);
    }

    @DeleteMapping("/{nroTransaction}")
    public ResponseEntity<String> eliminateForId(@PathVariable("nroTransaction") Long nroTransaction){
        boolean eliminate = transactionService.eliminateTransaction(nroTransaction);
        if (eliminate) {
            return ResponseEntity.ok("Transaction with Nro was deleted " + nroTransaction);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Could not find or delete transaction with Nro " + nroTransaction);
        }
    }

    @PutMapping("/{nroTransaction}")
    public ResponseEntity<TransactionModel> updateTransaction(
            @PathVariable("nroTransaction") Long nroTransaction,
            @RequestBody TransactionModel updatedTransaction
    ) throws Exception {
        TransactionModel updated = transactionService.updateTransaction(nroTransaction, updatedTransaction);
        if (updated != null) {
            return ResponseEntity.ok(updated);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
