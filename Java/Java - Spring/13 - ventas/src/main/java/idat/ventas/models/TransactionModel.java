package idat.ventas.models;

import jakarta.persistence.Id;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Column;

@Entity
@Table(name = "transaction")
public class TransactionModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long nroTransaction;
    private String client;
    private String product;
    private String amount;
    private String price;
    private String date;

    public TransactionModel(){}

    public TransactionModel(
            Long nroTransaction,
            String client,
            String product,
            String amount,
            String price,
            String date
    ) {
        this.nroTransaction = nroTransaction;
        this.client = client;
        this.product = product;
        this.amount = amount;
        this.price = price;
        this.date = date;
    }

    public Long getNroTransaction() {
        return nroTransaction;
    }

    public void setNroTransaction(Long nroTransaction) {
        this.nroTransaction = nroTransaction;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
