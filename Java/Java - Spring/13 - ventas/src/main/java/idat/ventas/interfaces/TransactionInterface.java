package idat.ventas.interfaces;

import idat.ventas.models.TransactionModel;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Optional;

@Service
public interface TransactionInterface {
    ArrayList<TransactionModel> obtainTransaction();

    TransactionModel saveTransaction(TransactionModel transaction);

    Optional<TransactionModel> obtationForId(Long id);

    ArrayList<TransactionModel> obtainForDate(String date);

    boolean eliminateTransaction(Long id);

    TransactionModel updateTransaction(Long id, TransactionModel updatedTransaction) throws Exception;
}
